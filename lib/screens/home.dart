import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tank_app/classes/device.dart';
import 'package:tank_app/screens/add_device.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final firestoreInstance = Firestore.instance;

  List<WaterData> devices = [];

  void _getInit() async {
    final prefs = await SharedPreferences.getInstance();

    List dev = prefs.getStringList("devices") ?? [];

    dev.forEach((element) {
      WaterData d = new WaterData(element);

      firestoreInstance
          .collection("devices")
          .document(element)
          .snapshots()
          .listen((value) {
        var waterLevel = value.data["waterLevel"];
        var tankDepth = value.data["tankDepth"];
        var waterLevelPercentage = waterLevel / tankDepth;
        d.getData(waterLevel, waterLevelPercentage);

        setState(() {
          devices.add(d);
        });
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Water Level'),
        leading: Container(),
      ),
      body: ListView.builder(
          itemCount: devices.length,
          itemBuilder: (context, index) {
            return Card(
              child: ListTile(
                leading: CircularProgressIndicator(
                  backgroundColor: Colors.indigo[50],
                  value: devices[index].waterLevelPercentage,
                ),
                title: Text(devices[index].waterLevel.toString()),
              ),
            );
          }),
      floatingActionButton: FlatButton.icon(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AddDeviceRoute()));
          },
          icon: Icon(Icons.add),
          label: Text('Add a new device')),
    );
  }
}
